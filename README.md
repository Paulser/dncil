# Diagrams.net - Custom Icons Librarys

A collection of custom icons sourced from third parties to improve Diagram.net (Draw.io) selection.

## Unifi Icons

You will find a full collection of images in our Marketing material page , divided by product line. You may also download 2017 Visio stencils and SVG icons attached below. There is also this great collection created by our Community user Bobby-B, don't forget to upvote him if you use his work! If that link is down, you can use the DropBox link he provides in this post.

Original Source: https://help.ui.com/hc/en-us/articles/204911374-Ubiquiti-Icons-and-Images-for-Diagrams

New Forum: https://community.ui.com/questions/VISIO-shapes-stencils/5a9c7873-0dc2-4e94-9a55-518bbe493da0#answer/f59de0fc-8179-4504-820b-598324df6827

## TP-Link Corporate Iconography

https://www.tp-link.com/uk/support/download/


## TP-Link 

https://community.tp-link.com/en/business/forum/topic/99688
